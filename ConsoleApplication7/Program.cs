﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication7
{
    class GameOfLife
    {
        protected char[,] board;
        protected char[,] copy;
        protected char alive = '$';
        protected char dead = '_';
        protected int size;
        public GameOfLife()
        {
            Random random = new Random(DateTime.Now.Millisecond);
            size = 10;
            board = new char[size+2, size+2];
            copy = new char[size+2, size+2];
            for (int i = 0; i < size+2; i++)
            {
                for (int j = 0; j < size+2; j++)
                {
                    if(i==0 || j==0 || i==size+1 || j==size+1)
                    {
                        board[i, j] = dead;
                        copy[i, j] = dead;
                    }
                    else if (random.Next(0, 2) == 0)
                    {
                        board[i, j] = dead;
                        copy[i, j] = dead;
                    }
                    else
                    {
                        board[i, j] = alive;
                        copy[i, j] = alive;
                    }
                }
            }
        }
        public GameOfLife(int s)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            size = s;
            board = new char[size + 2, size + 2];
            copy = new char[size + 2, size + 2];
            for (int i = 0; i < size + 2; i++)
            {
                for (int j = 0; j < size + 2; j++)
                {
                    if (i == 0 || j == 0 || i == size + 1 || j == size + 1)
                    {
                        board[i, j] = dead;
                        copy[i, j] = dead;
                    }
                    else if (random.Next(0, 2) == 0)
                    {
                        board[i, j] = dead;
                        copy[i, j] = dead;
                    }
                    else
                    {
                        board[i, j] = alive;
                        copy[i, j] = alive;
                    }
                }
            }
        }
        private int sasiedzi (int i, int j)
        {
            int count=0;
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    if (board[i + x, j + y] == alive)
                        if(!(x==0 && y==0))
                            count++;
                }
            }
            return count;

        }
        public void actualization()
        {
            for (int i = 1; i < size+1; i++)
            {
                for (int j = 1; j < size+1; j++)
                {
                    if (sasiedzi(i,j)<2 || sasiedzi(i,j)>3)
                    {
                        copy[i, j] = dead;
                    }
                    else if (sasiedzi(i,j) == 3)
                    {
                        copy[i, j] = alive;
                    }
                }
            }
            for (int i = 1; i < size+1; i++)
            {
                for (int j = 1; j < size+1; j++)
                {
                    board[i, j] = copy[i, j];
                }
            }
        }
        public void show()
        {
            for (int i = 1; i < size+1; i++)
            {
                for (int j = 1; j < size+1; j++)
                {
                    Console.Write(board[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n");
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            GameOfLife game = new GameOfLife(15);
            while(true)
            {
                game.show();
                Console.ReadLine();
                game.actualization();
            }

        }
    }
}
